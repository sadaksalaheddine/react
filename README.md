# React + Vite

Ce modèle fournit une configuration minimale pour faire fonctionner React avec Vite avec HMR et quelques règles ESLint.

Actuellement, deux plugins officiels sont disponibles :

- [@vitejs/plugin-react](https://github.com/vitejs/vite-plugin-react/blob/main/packages/plugin-react/README.md) utilise [Babel](https://babeljs.io/) pour le rafraîchissement rapide
- [@vitejs/plugin-react-swc](https://github.com/vitejs/vite-plugin-react-swc) utilise [SWC](https://swc.rs/) pour le rafraîchissement rapide

## Installation et Configuration

### Prérequis

- Docker
- Docker Compose

### Étapes

1. **Cloner le dépôt :**

    ```sh
    git clone https://gitlab.com/sadaksalaheddine/react.git
    cd react-vite
    ```

2. **Construire et démarrer les conteneurs Docker :**

    ```sh
    docker-compose up --build -d
    ```

3. **Accéder à l'application :**

   Ouvrez votre navigateur et allez à [http://localhost:5173](http://localhost:5173).

### Notes

- Assurez-vous que Docker et Docker Compose sont installés et en cours d'exécution sur votre machine.
- L'application sera accessible sur `http://localhost:5173` après avoir démarré les conteneurs Docker.
