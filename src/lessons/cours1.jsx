import {useState} from "react";

const Cours1 = () => {
    const [slh, setSlh] = useState(1);
    // const [count, setCount] = useState(0)
    const handleClick = () => {
        console.log("Button clicked");
            setSlh(slh + 1)
    }
    return (
        <div>
            <h1>Page Cours 1</h1>
            <button onClick={handleClick}>Click me {slh}</button>
        </div>
    );
}
export default Cours1;