import {useState, useRef} from "react";

const ListItems = () => {
    const items = {
        item1: 'Item 1',
        item2: 'Item 2',
        item3: 'Item 3',
        item4: 'Item 4',
        item5: 'Item 5',

    }

    const personnes = [
        {
            id: 1,
            nom: 'John Doe',
            age: 25
        },
        {
            id: 2,
            nom: 'salah',
            age: 30
        }
    ]

    const [cl, setCl] = useState(false);
    const inputRef = useRef(0);
    const handleClick = () => {
        console.log("Button clicked");
        setCl(!cl);
    }
    //const me = useRef(0);
    const handleClickUseRef = () => {
        console.log(inputRef)
    }


    return (
        <div>

            <h3>Test validation </h3>
            <p>Vous devez cocher la case</p>
            <input type="checkbox" onClick={handleClick}/>
            {cl ? <p>Case cochée</p> : <p>Case non cochée</p>}
            <br/>


            how to list this items with map function
            <ul>
                {Object.keys(items).map((key) => (
                    <li key={key}>{items[key]}</li>
                ))}
            </ul>

            <h2>Personnes list</h2>
            <ul>
                {personnes.map((personne) => (
                    <li key={personne.id}>{personne.nom} - {personne.age}</li>
                ))}
            </ul>
            <button onClick={handleClickUseRef}>use Ref example </button>
            <p>useRef value: {inputRef}</p>

        </div>
    )
}
export default ListItems;