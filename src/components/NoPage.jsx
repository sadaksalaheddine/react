const NoPage = () => {
    return (
        <div className="text-slate-50">
            <h1>404</h1>
            <p>Page Not Found</p>
        </div>
    );
}
export default NoPage;