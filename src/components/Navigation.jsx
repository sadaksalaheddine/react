import { Link } from 'react-router-dom';
const Navigation = () => {
    return (
        <nav className="flex justify-center bg-gray-800 p-4">
            <ul className="flex space-x-4">
                <li>
                    <Link to="/" className="text-white">Home</Link>
                </li>
                <li>
                    <Link to="/api" className="text-white">API Call</Link>
                </li>
                <li>
                    <Link to="/no-page" className="text-white">No Page</Link>
                </li>
            </ul>
        </nav>
    )
}
export default Navigation