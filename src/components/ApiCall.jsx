import React, {useState, useEffect} from 'react';



function ApiCall() {
    const [ApiState, setApiState] = useState({
        'loading': true,
        'error': false,
        'data': undefined,
    });

     let content;
     if (ApiState.loading) {
         content = <p>Loading...</p>;
     } else if (ApiState.error) {
            content = <p>Error occurred</p>;
     } else if (ApiState.data?.length > 0) {
            content = <ul>
                {ApiState.data.map((item) => (
                    <li key={item.id}>{item.name}</li>
                ))}
            </ul>
     }

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then((response) => response.json())
            .then((data) => {
                setApiState({
                    'loading': false,
                    'error': false,
                    'data': data,
                });
            })
            .catch((error) => {
                setApiState({
                    'loading': false,
                    'error': true,
                    'data': undefined,
                });
            });

    },[]);


    return (
        <div className="text-slate-50">
            <h1>API Call Result</h1>
           <button className="card" onClick={() => {
                setApiState.loading = true;
           }
              }>Refresh</button>
            {content}
        </div>
    );
}

export default ApiCall;