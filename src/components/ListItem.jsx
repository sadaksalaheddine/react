const ListItem = ({itemTodo,deleteTodo}) => {
    return (
        <li className="p2 bg-zinc-200 mb-2 rounded flex">
            {/* eslint-disable-next-line react/prop-types */}
            <span>{itemTodo.content}</span>
            <button className="ml-auto bg-red-600 w-6 h-6 rounded text-zinc-200" onClick={() => deleteTodo(itemTodo.id)}>X</button>
        </li>
    )
}
export default ListItem

