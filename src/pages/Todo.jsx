import ListItem from "../components/ListItem.jsx";
import {useState} from "react";
import {nanoid} from "nanoid";
const Todo = () => {
    const [todoList, setTodoList] = useState([
        {id:nanoid(5),content: "text1"},
        {id:nanoid(5),content: "text2"},
        {id:nanoid(5),content: "text3"},
        {id:nanoid(5),content: "text4"},
        {id:nanoid(5),content: "text5"},
        {id:nanoid(5),content: "text6"},
        {id:nanoid(5),content: "text7"},
        {id:nanoid(5),content: "text8"},
        {id:nanoid(5),content: "text9"},
        {id:nanoid(5),content: "text10"}]);

    const [todoItem, setTodoItem] = useState("");
    const [showError, setShowError] = useState(false);
    //add item
    const handleSubmit = (e) => {
        e.preventDefault();
        if (todoItem === "") {
            setShowError(true);
            return;
        }
        const newTodo = {
            id: nanoid(5),
            content: todoItem
        };
        setTodoList([...todoList, newTodo]);
        setTodoItem("");
        setShowError(false);

    };

    //delete Item
    const deleteTodo = (id) => {
        const newTodoList = todoList.filter(item => item.id !== id);
        setTodoList(newTodoList);

    }

    return (
        <>
            <h1 className="text-3xl text-slate-100 mb-4 ">La To-do liste</h1>
            <form className="mb-10" onSubmit={handleSubmit}>
                <label htmlFor="todo-item" className="text-slate-50">Ajouter une tâche</label>
                <input type="text" className="block w-full px-2 py-2 bg-slate-50 rounded mt-2"
                       value={todoItem}
                       onChange={(e) => setTodoItem(e.target.value)}
                />
                {showError && <p className="text-red-500 text-sm">Veuillez entrer une tâche</p>}
                <button className="mt-4 px-2 py-2 bg-slate-50 rounded min-w-[115px]">Ajouter</button>
            </form>
            <ul>
                {todoList.length > 0 && todoList.length === 0 && (
                    <li className="text-slate-50 text-md">pas d'items a afficher...</li>
                )}
                {
                    todoList.map(item => (
                        <ListItem key={item.id} itemTodo={item} deleteTodo={deleteTodo}/>
                    ))
                }
            </ul>
        </>
    )
}
export default Todo