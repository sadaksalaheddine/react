import Navigation from "./components/Navigation.jsx";
import ApiCall from "./components/ApiCall.jsx";
import { BrowserRouter,Routes, Route } from 'react-router-dom';
import NoPage from "./components/NoPage.jsx";
import Todo from "./pages/Todo.jsx";

function App() {


  return (
      <>
        <div className="h-screen">

            <BrowserRouter>
                <Navigation/>
                <Routes>
                        <Route path="/" element={<Todo />} />
                        <Route path="/api" element={<ApiCall />} />
                        <Route path="*" element={<NoPage/>} />
                        <Route path="/no-page" element={<NoPage/>} />
                </Routes>
            </BrowserRouter>
        </div>
      </>
  )
}

export default App
